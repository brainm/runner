package main

/**
* Runner script
* This script allow to create RPC server.
*
* INSTALLATION
* Create config file 'config.json':
*    {
*        "Host": "localhost",
*        "Port": 3000,
*        "Debug": true,
*        "BasicAuth": false,
*        "Username": "admin",
*        "Password": "password",
*        "Paths": [
*		    {
*               "Path": "/date",
*               "Exec": "date",
*               "Args": ["+%Y"],
*               "Result": ""
*            },
*		    {"Path": "/ping", "Exec": "ping", "Args": ["-c 4", "%host%"], "Result": ""}
*        ]
*    }
* Where Path is path on host, Exec executable script on programm to run, Args is array
* of arguments (you can user %placeholder% via _GET/_POST args)
*
* USAGE
* /date - return current year
* /ping?host=8.8.8.8 - ping server
*
 */
import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
)

type Configuration struct {
	Host      string
	Port      int
	Debug     bool
	BasicAuth bool
	Username  string
	Password  string
	Paths     []Exec
}

type Exec struct {
	Path   string
	Exec   string
	Args   []string
	Result string
}

var config Configuration

func main() {
	f, err := os.OpenFile("runner.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer f.Close()
	log.SetOutput(f)
	log.Print("Start")

	path := "config.json"
	if len(os.Args) > 1 {
		path = os.Args[1]
	}
	file, _ := os.Open(path)
	decoder := json.NewDecoder(file)
	config = Configuration{}
	err = decoder.Decode(&config)
	if err != nil {
		fmt.Println("Config not found, please using following defaults")
		fmt.Println("Exampale config.json")
		fmt.Println("\t{\"Host\": \"localhost\",\"Port\": 3000,\"Debug\":true,\"BasicAuth\":false,\"Username\":\"admin\",\"Password\":\"password\",\"Paths\": [{\"Path\": \"/date\", \"Exec\": \"date\", \"Args\": [\"+%Y\"], \"Result\": \"\"},{\"Path\": \"/ping\", \"Exec\": \"ping\", \"Args\": [\"-c 4\", \"%host%\"], \"Result\": \"\"}]}")
		os.Exit(1)
	}
	fmt.Println("Reading config file...")
	fmt.Println("Host: " + config.Host)
	fmt.Println("Port: " + strconv.Itoa(config.Port))
	fmt.Println("Parse scheme:")
	for _, path := range config.Paths {
		fmt.Print("\t")
		fmt.Println(path)
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if !config.BasicAuth || (config.BasicAuth && BasicAuth(w, r)) {
			execHandlers(config.Paths, w, r)
		}
	})

	log.Fatal(http.ListenAndServe(config.Host+":"+strconv.Itoa(config.Port), nil))
}

/**
* Map config for handlers
 */
func execHandlers(execs []Exec, w http.ResponseWriter, r *http.Request) {
	for _, exec := range execs {
		if exec.Path == r.URL.Path {
			var program = exec.Exec
			var args []string
			for _, arg := range exec.Args {
				var re = regexp.MustCompile(`%([^%]+)%`)
				var res = re.FindStringSubmatch(arg)
				if len(res) > 0 {
					var param = strings.TrimSpace(r.FormValue(res[1]))
					if param != "" {
						args = append(args, strings.Replace(arg, res[0], param, -1))
					}
				} else {
					args = append(args, arg)
				}
			}

			w.Header().Set("Access-Control-Allow-Origin", "*")

			//
			var output []byte
			var err error
			output, err = run(program, args)
			if err != nil {
				http.Error(w, string(output), http.StatusInternalServerError)
				return
			}
			// IF Accept request header start with 'application' or 'image' then response binary content
			if r.Header.Get("Accept") != "" && (strings.Index(r.Header.Get("Accept"), "application") == 0 || strings.Index(r.Header.Get("Accept"), "image") == 0) {
				w.Header().Set("Content-Type", r.Header.Get("Accept"))
				w.Write(output)
			} else {
				js, err := json.Marshal(Exec{Path: exec.Path, Exec: program, Args: args, Result: string(output)})
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				w.Header().Set("Content-Type", "application/json")
				w.Write(js)
			}

			//
			break
		}
	}
}

/**
* Run program with arguments
 */
func run(program string, args []string) ([]byte, error) {
	var (
		cmdOut []byte
		err    error
	)
	cmdName := program
	cmdArgs := args
	var cmdArgsString string
	for _, value := range cmdArgs {
		cmdArgsString += "`" + value + "` "
	}
	err = nil
	if cmdOut, err = exec.Command(cmdName, cmdArgs...).CombinedOutput(); err != nil {
		log.Print("ERROR: `" + cmdName + "` " + cmdArgsString + err.Error())
	} else {
		log.Print("OK: `" + cmdName + "` " + cmdArgsString)
	}
	if config.Debug {
		log.Print(string(cmdOut))
	}
	return cmdOut, err
}

/**
* Basic auth algorithms
 */
func BasicAuth(w http.ResponseWriter, r *http.Request) bool {
	var auth []string
	if len(r.Header["Authorization"]) == 0 {
		http.Error(w, "Bad syntax, basic authorization required", http.StatusBadRequest)
		return false
	}
	auth = strings.SplitN(r.Header["Authorization"][0], " ", 2)
	if len(auth) != 2 || auth[0] != "Basic" {
		http.Error(w, "Bad syntax", http.StatusBadRequest)
		return false
	}
	payload, _ := base64.StdEncoding.DecodeString(auth[1])
	pair := strings.SplitN(string(payload), ":", 2)
	if len(pair) != 2 || !Validate(pair[0], pair[1]) {
		http.Error(w, "Authorization failed", http.StatusUnauthorized)
		return false
	}
	return true
}

func Validate(username, password string) bool {
	if username == config.Username && password == config.Password {
		return true
	}
	return false
}
