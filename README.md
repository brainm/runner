#CONFIGURATION#
Use following template to create config

````javascript
{
    "Host": "localhost",
    "Port": 3000,
    "Debug": false,
    "BasicAuth": false,
    "Username": "admin",
    "Password": "password",
    "Paths": [
		{"Path": "/date", "Exec": "date", "Args": ["+%Y"], "Result": ""},
		{"Path": "/ping", "Exec": "ping", "Args": ["-c 4", "%host%"], "Result": ""}		
    ]
}
````
Where *Host* - hostname, leave blank to listen all interfaces;

*Debug* - log level;

*BasicAuth* - user basic auth algorithms;

*Path* - url path to function;

*Exec* - terminal command, bash script or external program;

*Args* - array of arguments, use placeholder '%mark%' to replace argument to GET/POST value.

#COMPILATION#

````bash
go build main.go
````

#USAGE#

Run program

````bash
/path/to/program/main /path/to/program/config.json
````

Call link 

````bash
curl http://localhost:3000/date
curl -u "admin:password" http://localhost:3000/date
````

Result

````javascript
{"Path":"/date","Exec":"date","Args":["+%Y"],"Result":"2015\n"}
````

IF you want to receive binary content instead of json, set request header to 'Accept' to 'application...' or 'image...'.

#AJAX call#

You can run query to program in jQuery:

````javascript
jQuery.ajax({
	type: "POST",
	url: "http://localhost:3000/ping",
	data: {"host": "8.8.8.8"},
	success: function(data){
		console.log('success')
	},
	error: function(data){
		console.log('error')
	}
})
````

